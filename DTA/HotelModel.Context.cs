﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DTA
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class HotelMNSEntities : DbContext
    {
        public HotelMNSEntities()
            : base("name=HotelMNSEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Booking> Bookings { get; set; }
        public virtual DbSet<Booking_Room> Booking_Room { get; set; }
        public virtual DbSet<Booking_Service> Booking_Service { get; set; }
        public virtual DbSet<Booking_Status> Booking_Status { get; set; }
        public virtual DbSet<Cancel_Booking> Cancel_Booking { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Facility> Facilities { get; set; }
        public virtual DbSet<Invoice> Invoices { get; set; }
        public virtual DbSet<Occupied_Room> Occupied_Room { get; set; }
        public virtual DbSet<Room> Rooms { get; set; }
        public virtual DbSet<Room_Capacility> Room_Capacility { get; set; }
        public virtual DbSet<Room_Category> Room_Category { get; set; }
        public virtual DbSet<Room_Facilities> Room_Facilities { get; set; }
        public virtual DbSet<Room_Image> Room_Image { get; set; }
        public virtual DbSet<Room_Service> Room_Service { get; set; }
        public virtual DbSet<Room_Status> Room_Status { get; set; }
        public virtual DbSet<Room_Type> Room_Type { get; set; }
        public virtual DbSet<Service> Services { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<User_Level> User_Level { get; set; }
    
        public virtual int spBookingStatusCheckOut(Nullable<int> bookingid)
        {
            var bookingidParameter = bookingid.HasValue ?
                new ObjectParameter("bookingid", bookingid) :
                new ObjectParameter("bookingid", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("spBookingStatusCheckOut", bookingidParameter);
        }
    
        public virtual ObjectResult<spGetAllCustomerBookingConfirm_Result> spGetAllCustomerBookingConfirm()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<spGetAllCustomerBookingConfirm_Result>("spGetAllCustomerBookingConfirm");
        }
    
        public virtual ObjectResult<spGetCustomerJustBooking_Result> spGetCustomerJustBooking(Nullable<int> bookingId)
        {
            var bookingIdParameter = bookingId.HasValue ?
                new ObjectParameter("bookingId", bookingId) :
                new ObjectParameter("bookingId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<spGetCustomerJustBooking_Result>("spGetCustomerJustBooking", bookingIdParameter);
        }
    
        public virtual ObjectResult<spGetInfoOfRoomOccupied_Result> spGetInfoOfRoomOccupied(Nullable<int> roomid, Nullable<int> bookingid)
        {
            var roomidParameter = roomid.HasValue ?
                new ObjectParameter("roomid", roomid) :
                new ObjectParameter("roomid", typeof(int));
    
            var bookingidParameter = bookingid.HasValue ?
                new ObjectParameter("bookingid", bookingid) :
                new ObjectParameter("bookingid", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<spGetInfoOfRoomOccupied_Result>("spGetInfoOfRoomOccupied", roomidParameter, bookingidParameter);
        }
    
        public virtual ObjectResult<spGetInvoice_Result> spGetInvoice(Nullable<int> bookingId)
        {
            var bookingIdParameter = bookingId.HasValue ?
                new ObjectParameter("bookingId", bookingId) :
                new ObjectParameter("bookingId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<spGetInvoice_Result>("spGetInvoice", bookingIdParameter);
        }
    
        public virtual ObjectResult<spGetlistroomCustomerOccupied_Result> spGetlistroomCustomerOccupied(Nullable<int> bookingid)
        {
            var bookingidParameter = bookingid.HasValue ?
                new ObjectParameter("bookingid", bookingid) :
                new ObjectParameter("bookingid", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<spGetlistroomCustomerOccupied_Result>("spGetlistroomCustomerOccupied", bookingidParameter);
        }
    
        public virtual ObjectResult<spGetListRoomOccupied_Result> spGetListRoomOccupied()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<spGetListRoomOccupied_Result>("spGetListRoomOccupied");
        }
    
        public virtual ObjectResult<spGetListServiceAvaiableBooking_Result> spGetListServiceAvaiableBooking(Nullable<int> roomid)
        {
            var roomidParameter = roomid.HasValue ?
                new ObjectParameter("roomid", roomid) :
                new ObjectParameter("roomid", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<spGetListServiceAvaiableBooking_Result>("spGetListServiceAvaiableBooking", roomidParameter);
        }
    
        public virtual ObjectResult<spGetlistServiceRoom_Result> spGetlistServiceRoom(Nullable<int> booking)
        {
            var bookingParameter = booking.HasValue ?
                new ObjectParameter("booking", booking) :
                new ObjectParameter("booking", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<spGetlistServiceRoom_Result>("spGetlistServiceRoom", bookingParameter);
        }
    
        public virtual ObjectResult<spGetRoomBooked_Result> spGetRoomBooked(Nullable<int> booking)
        {
            var bookingParameter = booking.HasValue ?
                new ObjectParameter("booking", booking) :
                new ObjectParameter("booking", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<spGetRoomBooked_Result>("spGetRoomBooked", bookingParameter);
        }
    
        public virtual ObjectResult<Nullable<int>> spGetRoomId(Nullable<System.DateTime> startDate, Nullable<System.DateTime> endDate, Nullable<int> roomtypeid, Nullable<int> roomCategoryid)
        {
            var startDateParameter = startDate.HasValue ?
                new ObjectParameter("startDate", startDate) :
                new ObjectParameter("startDate", typeof(System.DateTime));
    
            var endDateParameter = endDate.HasValue ?
                new ObjectParameter("endDate", endDate) :
                new ObjectParameter("endDate", typeof(System.DateTime));
    
            var roomtypeidParameter = roomtypeid.HasValue ?
                new ObjectParameter("roomtypeid", roomtypeid) :
                new ObjectParameter("roomtypeid", typeof(int));
    
            var roomCategoryidParameter = roomCategoryid.HasValue ?
                new ObjectParameter("roomCategoryid", roomCategoryid) :
                new ObjectParameter("roomCategoryid", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<int>>("spGetRoomId", startDateParameter, endDateParameter, roomtypeidParameter, roomCategoryidParameter);
        }
    
        public virtual int spUpdateBookingCheckIn(Nullable<int> bookingid, string id_card, string id_cardtype)
        {
            var bookingidParameter = bookingid.HasValue ?
                new ObjectParameter("bookingid", bookingid) :
                new ObjectParameter("bookingid", typeof(int));
    
            var id_cardParameter = id_card != null ?
                new ObjectParameter("id_card", id_card) :
                new ObjectParameter("id_card", typeof(string));
    
            var id_cardtypeParameter = id_cardtype != null ?
                new ObjectParameter("id_cardtype", id_cardtype) :
                new ObjectParameter("id_cardtype", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("spUpdateBookingCheckIn", bookingidParameter, id_cardParameter, id_cardtypeParameter);
        }
    
        public virtual int spUpdateCustomerBookingStatus(Nullable<int> bookingid)
        {
            var bookingidParameter = bookingid.HasValue ?
                new ObjectParameter("bookingid", bookingid) :
                new ObjectParameter("bookingid", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("spUpdateCustomerBookingStatus", bookingidParameter);
        }
    
        public virtual int spUpdateServiceInvoice(Nullable<int> bookingid, Nullable<int> totalservice, Nullable<decimal> totalpriceservice, Nullable<decimal> totalprice, Nullable<decimal> totalcustomermoney, Nullable<decimal> totalchange)
        {
            var bookingidParameter = bookingid.HasValue ?
                new ObjectParameter("bookingid", bookingid) :
                new ObjectParameter("bookingid", typeof(int));
    
            var totalserviceParameter = totalservice.HasValue ?
                new ObjectParameter("totalservice", totalservice) :
                new ObjectParameter("totalservice", typeof(int));
    
            var totalpriceserviceParameter = totalpriceservice.HasValue ?
                new ObjectParameter("totalpriceservice", totalpriceservice) :
                new ObjectParameter("totalpriceservice", typeof(decimal));
    
            var totalpriceParameter = totalprice.HasValue ?
                new ObjectParameter("totalprice", totalprice) :
                new ObjectParameter("totalprice", typeof(decimal));
    
            var totalcustomermoneyParameter = totalcustomermoney.HasValue ?
                new ObjectParameter("totalcustomermoney", totalcustomermoney) :
                new ObjectParameter("totalcustomermoney", typeof(decimal));
    
            var totalchangeParameter = totalchange.HasValue ?
                new ObjectParameter("totalchange", totalchange) :
                new ObjectParameter("totalchange", typeof(decimal));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("spUpdateServiceInvoice", bookingidParameter, totalserviceParameter, totalpriceserviceParameter, totalpriceParameter, totalcustomermoneyParameter, totalchangeParameter);
        }
    
        public virtual ObjectResult<spGetRoomOccupied_Result> spGetRoomOccupied()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<spGetRoomOccupied_Result>("spGetRoomOccupied");
        }
    }
}
