﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace DTA
{
    public class RoomMapDB
    {
        HotelMNSEntities db = new HotelMNSEntities();

        public List<spGetListRoomOccupied_Result> GetListRoomOccupied()
        {
            return db.spGetListRoomOccupied().ToList();
        }
        public spGetInfoOfRoomOccupied_Result GetInfo(int? roomid, int? bookingid)
        {
            return db.spGetInfoOfRoomOccupied(roomid, bookingid).Single();
        }
        public List<spGetListServiceAvaiableBooking_Result> GetListService(int? roomid)
        {
            return db.spGetListServiceAvaiableBooking(roomid).ToList();
        }
        public void InsertBookingService(int bookingid, int roomid, int serviceid)
        {
            //insert booking Service
            Booking_Service bookingService = new Booking_Service();
            bookingService.Booking = bookingid;
            bookingService.Room = roomid;
            bookingService.Service = serviceid;
            db.Booking_Service.Add(bookingService);
            db.SaveChanges();

        }

        public void UpdateInvoice(int? bookingid, int? totalservice, decimal? totalpriceservice, decimal? customerpaid, decimal? change_money)
        {
            spGetInvoice_Result Invoice = db.spGetInvoice(bookingid).Single();
            totalservice += Invoice.total_service;
            totalpriceservice += Invoice.total_priceservice;
            customerpaid += Invoice.total_customerpaid;
            change_money += Invoice.chaneg_money;
            decimal? totalprice = Invoice.totalprice + totalpriceservice;
            db.spUpdateServiceInvoice(bookingid, totalservice, totalpriceservice, totalprice, customerpaid, change_money);
        }

        public List<spGetRoomOccupied_Result> GetRoom()
        {
            return db.spGetRoomOccupied().ToList();
        }
        public List<spGetlistroomCustomerOccupied_Result> GetReallyRoom(int? bookingid)
        {
            return db.spGetlistroomCustomerOccupied(bookingid).ToList();
        }
        public List<spGetlistServiceRoom_Result> GetService(int? bookingid)
        {
            return db.spGetlistServiceRoom(bookingid).ToList();
        }
        public void UpdateBookingCheckOut(int? bookingid)
        {
            db.spBookingStatusCheckOut(bookingid);
        }
    }
}
