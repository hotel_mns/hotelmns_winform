﻿namespace HotelMNS_WF.Shared.Accomodation
{
    partial class btnCheckOut
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            MetroSuite.MetroTextBox.MainColorScheme mainColorScheme2 = new MetroSuite.MetroTextBox.MainColorScheme();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(btnCheckOut));
            this.label1 = new System.Windows.Forms.Label();
            this.txtSearchRoom = new MetroSuite.MetroTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCustomerName = new System.Windows.Forms.TextBox();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel4 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel5 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel6 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel7 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel8 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel9 = new MaterialSkin.Controls.MaterialLabel();
            this.label4 = new System.Windows.Forms.Label();
            this.chkboxStAvaiable = new MaterialSkin.Controls.MaterialCheckBox();
            this.chkboxStFixing = new MaterialSkin.Controls.MaterialCheckBox();
            this.chkboxStCusWalking = new MaterialSkin.Controls.MaterialCheckBox();
            this.chkboxStDirty = new MaterialSkin.Controls.MaterialCheckBox();
            this.chkboxStCleaning = new MaterialSkin.Controls.MaterialCheckBox();
            this.chkboxStOccupied = new MaterialSkin.Controls.MaterialCheckBox();
            this.btnEditInfo = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.radMale = new MetroFramework.Controls.MetroRadioButton();
            this.radFemale = new MetroFramework.Controls.MetroRadioButton();
            this.materialLabel11 = new MaterialSkin.Controls.MaterialLabel();
            this.txtBookingId = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtIdCard = new System.Windows.Forms.TextBox();
            this.txtPhoneNumber = new System.Windows.Forms.TextBox();
            this.txtNotes = new System.Windows.Forms.TextBox();
            this.materialLabel12 = new MaterialSkin.Controls.MaterialLabel();
            this.txtCitizenship = new System.Windows.Forms.TextBox();
            this.materialLabel13 = new MaterialSkin.Controls.MaterialLabel();
            this.lbRoomNumber = new MaterialSkin.Controls.MaterialLabel();
            this.dgvListRoom = new System.Windows.Forms.DataGridView();
            this.txtIdCardType = new System.Windows.Forms.TextBox();
            this.datepickCheckIn = new MetroFramework.Controls.MetroDateTime();
            this.datepickCheckOut = new MetroFramework.Controls.MetroDateTime();
            this.btnCheckOutCustomer = new System.Windows.Forms.Button();
            this.btnCheckIn = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListRoom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(165, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "Room Mapping";
            // 
            // txtSearchRoom
            // 
            this.txtSearchRoom.BanChars = false;
            mainColorScheme2.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(164)))), ((int)(((byte)(240)))));
            mainColorScheme2.BorderColorNormal = System.Drawing.Color.FromArgb(((int)(((byte)(96)))), ((int)(((byte)(125)))), ((int)(((byte)(139)))));
            mainColorScheme2.MainColor = System.Drawing.Color.White;
            this.txtSearchRoom.ColorScheme = mainColorScheme2;
            this.txtSearchRoom.DefaultText = null;
            this.txtSearchRoom.DefaultTextColor = System.Drawing.Color.LightGray;
            this.txtSearchRoom.DefaultTextNormalForeColor = System.Drawing.SystemColors.ControlText;
            this.txtSearchRoom.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearchRoom.ForeColor = System.Drawing.Color.Black;
            this.txtSearchRoom.IllegalChars = ((System.Collections.Generic.List<char>)(resources.GetObject("txtSearchRoom.IllegalChars")));
            this.txtSearchRoom.Location = new System.Drawing.Point(270, 55);
            this.txtSearchRoom.Name = "txtSearchRoom";
            this.txtSearchRoom.Size = new System.Drawing.Size(662, 30);
            this.txtSearchRoom.TabIndex = 3;
            this.txtSearchRoom.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtSearchRoom.UseDefaultText = false;
            this.txtSearchRoom.TextChanged += new System.EventHandler(this.txtSearchRoom_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(163, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 19);
            this.label2.TabIndex = 5;
            this.label2.Text = "Search Room :";
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(68, 295);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(146, 19);
            this.materialLabel1.TabIndex = 6;
            this.materialLabel1.Text = "Cusstomer\'s Name :";
            // 
            // label3
            // 
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Location = new System.Drawing.Point(79, 280);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(991, 2);
            this.label3.TabIndex = 7;
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustomerName.Location = new System.Drawing.Point(72, 317);
            this.txtCustomerName.Multiline = true;
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.Size = new System.Drawing.Size(204, 30);
            this.txtCustomerName.TabIndex = 8;
            this.txtCustomerName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // materialLabel2
            // 
            this.materialLabel2.AutoSize = true;
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel2.Location = new System.Drawing.Point(309, 295);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(64, 19);
            this.materialLabel2.TabIndex = 9;
            this.materialLabel2.Text = "Id Card :";
            // 
            // materialLabel3
            // 
            this.materialLabel3.AutoSize = true;
            this.materialLabel3.Depth = 0;
            this.materialLabel3.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel3.Location = new System.Drawing.Point(309, 358);
            this.materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel3.Name = "materialLabel3";
            this.materialLabel3.Size = new System.Drawing.Size(84, 19);
            this.materialLabel3.TabIndex = 11;
            this.materialLabel3.Text = "Card Type :";
            // 
            // materialLabel4
            // 
            this.materialLabel4.AutoSize = true;
            this.materialLabel4.Depth = 0;
            this.materialLabel4.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel4.Location = new System.Drawing.Point(68, 358);
            this.materialLabel4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel4.Name = "materialLabel4";
            this.materialLabel4.Size = new System.Drawing.Size(86, 19);
            this.materialLabel4.TabIndex = 13;
            this.materialLabel4.Text = "Booking ID:";
            // 
            // materialLabel5
            // 
            this.materialLabel5.AutoSize = true;
            this.materialLabel5.Depth = 0;
            this.materialLabel5.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel5.Location = new System.Drawing.Point(309, 490);
            this.materialLabel5.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel5.Name = "materialLabel5";
            this.materialLabel5.Size = new System.Drawing.Size(47, 19);
            this.materialLabel5.TabIndex = 15;
            this.materialLabel5.Text = "Email";
            // 
            // materialLabel6
            // 
            this.materialLabel6.AutoSize = true;
            this.materialLabel6.Depth = 0;
            this.materialLabel6.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel6.Location = new System.Drawing.Point(309, 420);
            this.materialLabel6.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel6.Name = "materialLabel6";
            this.materialLabel6.Size = new System.Drawing.Size(116, 19);
            this.materialLabel6.TabIndex = 17;
            this.materialLabel6.Text = "Phone Number :";
            // 
            // materialLabel7
            // 
            this.materialLabel7.AutoSize = true;
            this.materialLabel7.Depth = 0;
            this.materialLabel7.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel7.Location = new System.Drawing.Point(68, 421);
            this.materialLabel7.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel7.Name = "materialLabel7";
            this.materialLabel7.Size = new System.Drawing.Size(110, 19);
            this.materialLabel7.TabIndex = 19;
            this.materialLabel7.Text = "Date Check In :";
            // 
            // materialLabel8
            // 
            this.materialLabel8.AutoSize = true;
            this.materialLabel8.Depth = 0;
            this.materialLabel8.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel8.Location = new System.Drawing.Point(68, 490);
            this.materialLabel8.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel8.Name = "materialLabel8";
            this.materialLabel8.Size = new System.Drawing.Size(121, 19);
            this.materialLabel8.TabIndex = 21;
            this.materialLabel8.Text = "Date Check Out :";
            // 
            // materialLabel9
            // 
            this.materialLabel9.AutoSize = true;
            this.materialLabel9.Depth = 0;
            this.materialLabel9.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel9.Location = new System.Drawing.Point(553, 295);
            this.materialLabel9.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel9.Name = "materialLabel9";
            this.materialLabel9.Size = new System.Drawing.Size(58, 19);
            this.materialLabel9.TabIndex = 23;
            this.materialLabel9.Text = "Notes :";
            // 
            // label4
            // 
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Location = new System.Drawing.Point(816, 298);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(2, 187);
            this.label4.TabIndex = 25;
            // 
            // chkboxStAvaiable
            // 
            this.chkboxStAvaiable.AutoSize = true;
            this.chkboxStAvaiable.Depth = 0;
            this.chkboxStAvaiable.Font = new System.Drawing.Font("Roboto", 10F);
            this.chkboxStAvaiable.Location = new System.Drawing.Point(836, 320);
            this.chkboxStAvaiable.Margin = new System.Windows.Forms.Padding(0);
            this.chkboxStAvaiable.MouseLocation = new System.Drawing.Point(-1, -1);
            this.chkboxStAvaiable.MouseState = MaterialSkin.MouseState.HOVER;
            this.chkboxStAvaiable.Name = "chkboxStAvaiable";
            this.chkboxStAvaiable.Ripple = true;
            this.chkboxStAvaiable.Size = new System.Drawing.Size(83, 30);
            this.chkboxStAvaiable.TabIndex = 27;
            this.chkboxStAvaiable.Text = "Avaiable";
            this.chkboxStAvaiable.UseVisualStyleBackColor = true;
            // 
            // chkboxStFixing
            // 
            this.chkboxStFixing.AutoSize = true;
            this.chkboxStFixing.Depth = 0;
            this.chkboxStFixing.Font = new System.Drawing.Font("Roboto", 10F);
            this.chkboxStFixing.Location = new System.Drawing.Point(836, 357);
            this.chkboxStFixing.Margin = new System.Windows.Forms.Padding(0);
            this.chkboxStFixing.MouseLocation = new System.Drawing.Point(-1, -1);
            this.chkboxStFixing.MouseState = MaterialSkin.MouseState.HOVER;
            this.chkboxStFixing.Name = "chkboxStFixing";
            this.chkboxStFixing.Ripple = true;
            this.chkboxStFixing.Size = new System.Drawing.Size(67, 30);
            this.chkboxStFixing.TabIndex = 28;
            this.chkboxStFixing.Text = "Fixing";
            this.chkboxStFixing.UseVisualStyleBackColor = true;
            // 
            // chkboxStCusWalking
            // 
            this.chkboxStCusWalking.AutoSize = true;
            this.chkboxStCusWalking.Depth = 0;
            this.chkboxStCusWalking.Font = new System.Drawing.Font("Roboto", 10F);
            this.chkboxStCusWalking.Location = new System.Drawing.Point(836, 398);
            this.chkboxStCusWalking.Margin = new System.Windows.Forms.Padding(0);
            this.chkboxStCusWalking.MouseLocation = new System.Drawing.Point(-1, -1);
            this.chkboxStCusWalking.MouseState = MaterialSkin.MouseState.HOVER;
            this.chkboxStCusWalking.Name = "chkboxStCusWalking";
            this.chkboxStCusWalking.Ripple = true;
            this.chkboxStCusWalking.Size = new System.Drawing.Size(143, 30);
            this.chkboxStCusWalking.TabIndex = 30;
            this.chkboxStCusWalking.Text = "Customer Walking";
            this.chkboxStCusWalking.UseVisualStyleBackColor = true;
            // 
            // chkboxStDirty
            // 
            this.chkboxStDirty.AutoSize = true;
            this.chkboxStDirty.Depth = 0;
            this.chkboxStDirty.Font = new System.Drawing.Font("Roboto", 10F);
            this.chkboxStDirty.Location = new System.Drawing.Point(1006, 398);
            this.chkboxStDirty.Margin = new System.Windows.Forms.Padding(0);
            this.chkboxStDirty.MouseLocation = new System.Drawing.Point(-1, -1);
            this.chkboxStDirty.MouseState = MaterialSkin.MouseState.HOVER;
            this.chkboxStDirty.Name = "chkboxStDirty";
            this.chkboxStDirty.Ripple = true;
            this.chkboxStDirty.Size = new System.Drawing.Size(59, 30);
            this.chkboxStDirty.TabIndex = 33;
            this.chkboxStDirty.Text = "Dirty";
            this.chkboxStDirty.UseVisualStyleBackColor = true;
            // 
            // chkboxStCleaning
            // 
            this.chkboxStCleaning.AutoSize = true;
            this.chkboxStCleaning.Depth = 0;
            this.chkboxStCleaning.Font = new System.Drawing.Font("Roboto", 10F);
            this.chkboxStCleaning.Location = new System.Drawing.Point(1006, 357);
            this.chkboxStCleaning.Margin = new System.Windows.Forms.Padding(0);
            this.chkboxStCleaning.MouseLocation = new System.Drawing.Point(-1, -1);
            this.chkboxStCleaning.MouseState = MaterialSkin.MouseState.HOVER;
            this.chkboxStCleaning.Name = "chkboxStCleaning";
            this.chkboxStCleaning.Ripple = true;
            this.chkboxStCleaning.Size = new System.Drawing.Size(84, 30);
            this.chkboxStCleaning.TabIndex = 32;
            this.chkboxStCleaning.Text = "Cleaning";
            this.chkboxStCleaning.UseVisualStyleBackColor = true;
            // 
            // chkboxStOccupied
            // 
            this.chkboxStOccupied.AutoSize = true;
            this.chkboxStOccupied.Depth = 0;
            this.chkboxStOccupied.Font = new System.Drawing.Font("Roboto", 10F);
            this.chkboxStOccupied.Location = new System.Drawing.Point(1006, 320);
            this.chkboxStOccupied.Margin = new System.Windows.Forms.Padding(0);
            this.chkboxStOccupied.MouseLocation = new System.Drawing.Point(-1, -1);
            this.chkboxStOccupied.MouseState = MaterialSkin.MouseState.HOVER;
            this.chkboxStOccupied.Name = "chkboxStOccupied";
            this.chkboxStOccupied.Ripple = true;
            this.chkboxStOccupied.Size = new System.Drawing.Size(88, 30);
            this.chkboxStOccupied.TabIndex = 31;
            this.chkboxStOccupied.Text = "Occupied";
            this.chkboxStOccupied.UseVisualStyleBackColor = true;
            // 
            // btnEditInfo
            // 
            this.btnEditInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(230)))), ((int)(((byte)(118)))));
            this.btnEditInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditInfo.ForeColor = System.Drawing.Color.White;
            this.btnEditInfo.Location = new System.Drawing.Point(715, 512);
            this.btnEditInfo.Name = "btnEditInfo";
            this.btnEditInfo.Size = new System.Drawing.Size(133, 37);
            this.btnEditInfo.TabIndex = 34;
            this.btnEditInfo.Text = "Edit";
            this.btnEditInfo.UseVisualStyleBackColor = false;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(188)))), ((int)(((byte)(212)))));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(854, 513);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(133, 37);
            this.btnSave.TabIndex = 35;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            // 
            // label5
            // 
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Location = new System.Drawing.Point(836, 433);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(292, 2);
            this.label5.TabIndex = 36;
            // 
            // radMale
            // 
            this.radMale.AutoSize = true;
            this.radMale.Location = new System.Drawing.Point(911, 470);
            this.radMale.Name = "radMale";
            this.radMale.Size = new System.Drawing.Size(49, 15);
            this.radMale.TabIndex = 37;
            this.radMale.Text = "Male";
            this.radMale.UseSelectable = true;
            // 
            // radFemale
            // 
            this.radFemale.AutoSize = true;
            this.radFemale.Location = new System.Drawing.Point(966, 470);
            this.radFemale.Name = "radFemale";
            this.radFemale.Size = new System.Drawing.Size(61, 15);
            this.radFemale.TabIndex = 38;
            this.radFemale.Text = "Female";
            this.radFemale.UseSelectable = true;
            // 
            // materialLabel11
            // 
            this.materialLabel11.AutoSize = true;
            this.materialLabel11.Depth = 0;
            this.materialLabel11.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel11.Location = new System.Drawing.Point(841, 466);
            this.materialLabel11.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel11.Name = "materialLabel11";
            this.materialLabel11.Size = new System.Drawing.Size(64, 19);
            this.materialLabel11.TabIndex = 44;
            this.materialLabel11.Text = "Gender :";
            // 
            // txtBookingId
            // 
            this.txtBookingId.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBookingId.Location = new System.Drawing.Point(72, 380);
            this.txtBookingId.Multiline = true;
            this.txtBookingId.Name = "txtBookingId";
            this.txtBookingId.Size = new System.Drawing.Size(204, 30);
            this.txtBookingId.TabIndex = 46;
            this.txtBookingId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtEmail
            // 
            this.txtEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.Location = new System.Drawing.Point(313, 512);
            this.txtEmail.Multiline = true;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(204, 30);
            this.txtEmail.TabIndex = 48;
            this.txtEmail.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtIdCard
            // 
            this.txtIdCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIdCard.Location = new System.Drawing.Point(313, 317);
            this.txtIdCard.Multiline = true;
            this.txtIdCard.Name = "txtIdCard";
            this.txtIdCard.Size = new System.Drawing.Size(204, 30);
            this.txtIdCard.TabIndex = 47;
            this.txtIdCard.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtPhoneNumber
            // 
            this.txtPhoneNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhoneNumber.Location = new System.Drawing.Point(313, 442);
            this.txtPhoneNumber.Multiline = true;
            this.txtPhoneNumber.Name = "txtPhoneNumber";
            this.txtPhoneNumber.Size = new System.Drawing.Size(204, 30);
            this.txtPhoneNumber.TabIndex = 49;
            this.txtPhoneNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtNotes
            // 
            this.txtNotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNotes.Location = new System.Drawing.Point(557, 318);
            this.txtNotes.Multiline = true;
            this.txtNotes.Name = "txtNotes";
            this.txtNotes.Size = new System.Drawing.Size(204, 157);
            this.txtNotes.TabIndex = 52;
            // 
            // materialLabel12
            // 
            this.materialLabel12.AutoSize = true;
            this.materialLabel12.Depth = 0;
            this.materialLabel12.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel12.Location = new System.Drawing.Point(827, 443);
            this.materialLabel12.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel12.Name = "materialLabel12";
            this.materialLabel12.Size = new System.Drawing.Size(91, 19);
            this.materialLabel12.TabIndex = 53;
            this.materialLabel12.Text = "Citizenship :";
            // 
            // txtCitizenship
            // 
            this.txtCitizenship.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCitizenship.Location = new System.Drawing.Point(909, 440);
            this.txtCitizenship.Multiline = true;
            this.txtCitizenship.Name = "txtCitizenship";
            this.txtCitizenship.Size = new System.Drawing.Size(204, 24);
            this.txtCitizenship.TabIndex = 54;
            this.txtCitizenship.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // materialLabel13
            // 
            this.materialLabel13.AutoSize = true;
            this.materialLabel13.Depth = 0;
            this.materialLabel13.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel13.Location = new System.Drawing.Point(833, 295);
            this.materialLabel13.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel13.Name = "materialLabel13";
            this.materialLabel13.Size = new System.Drawing.Size(114, 19);
            this.materialLabel13.TabIndex = 55;
            this.materialLabel13.Text = "Room Number :";
            // 
            // lbRoomNumber
            // 
            this.lbRoomNumber.AutoSize = true;
            this.lbRoomNumber.Depth = 0;
            this.lbRoomNumber.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbRoomNumber.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbRoomNumber.Location = new System.Drawing.Point(947, 295);
            this.lbRoomNumber.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbRoomNumber.Name = "lbRoomNumber";
            this.lbRoomNumber.Size = new System.Drawing.Size(33, 19);
            this.lbRoomNumber.TabIndex = 56;
            this.lbRoomNumber.Text = "101";
            // 
            // dgvListRoom
            // 
            this.dgvListRoom.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvListRoom.BackgroundColor = System.Drawing.Color.White;
            this.dgvListRoom.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvListRoom.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListRoom.Location = new System.Drawing.Point(111, 108);
            this.dgvListRoom.Name = "dgvListRoom";
            this.dgvListRoom.Size = new System.Drawing.Size(939, 160);
            this.dgvListRoom.TabIndex = 57;
            this.dgvListRoom.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvListRoomMap_CellEnter);
            this.dgvListRoom.SelectionChanged += new System.EventHandler(this.dgvListRoomMap_SelectionChanged);
            this.dgvListRoom.DoubleClick += new System.EventHandler(this.dgvListRoom_DoubleClick);
            // 
            // txtIdCardType
            // 
            this.txtIdCardType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIdCardType.Location = new System.Drawing.Point(313, 380);
            this.txtIdCardType.Multiline = true;
            this.txtIdCardType.Name = "txtIdCardType";
            this.txtIdCardType.Size = new System.Drawing.Size(204, 30);
            this.txtIdCardType.TabIndex = 61;
            this.txtIdCardType.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // datepickCheckIn
            // 
            this.datepickCheckIn.Location = new System.Drawing.Point(72, 445);
            this.datepickCheckIn.MinimumSize = new System.Drawing.Size(0, 29);
            this.datepickCheckIn.Name = "datepickCheckIn";
            this.datepickCheckIn.Size = new System.Drawing.Size(204, 29);
            this.datepickCheckIn.TabIndex = 62;
            // 
            // datepickCheckOut
            // 
            this.datepickCheckOut.Location = new System.Drawing.Point(72, 513);
            this.datepickCheckOut.MinimumSize = new System.Drawing.Size(0, 29);
            this.datepickCheckOut.Name = "datepickCheckOut";
            this.datepickCheckOut.Size = new System.Drawing.Size(204, 29);
            this.datepickCheckOut.TabIndex = 63;
            // 
            // btnCheckOutCustomer
            // 
            this.btnCheckOutCustomer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(67)))), ((int)(((byte)(54)))));
            this.btnCheckOutCustomer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCheckOutCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCheckOutCustomer.ForeColor = System.Drawing.Color.White;
            this.btnCheckOutCustomer.Location = new System.Drawing.Point(993, 513);
            this.btnCheckOutCustomer.Name = "btnCheckOutCustomer";
            this.btnCheckOutCustomer.Size = new System.Drawing.Size(133, 37);
            this.btnCheckOutCustomer.TabIndex = 64;
            this.btnCheckOutCustomer.Text = "CheckOut";
            this.btnCheckOutCustomer.UseVisualStyleBackColor = false;
            this.btnCheckOutCustomer.Click += new System.EventHandler(this.btnCheckOutCustomer_Click);
            // 
            // btnCheckIn
            // 
            this.btnCheckIn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(152)))), ((int)(((byte)(0)))));
            this.btnCheckIn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCheckIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCheckIn.ForeColor = System.Drawing.Color.White;
            this.btnCheckIn.Location = new System.Drawing.Point(576, 513);
            this.btnCheckIn.Name = "btnCheckIn";
            this.btnCheckIn.Size = new System.Drawing.Size(133, 37);
            this.btnCheckIn.TabIndex = 65;
            this.btnCheckIn.Text = "Check In";
            this.btnCheckIn.UseVisualStyleBackColor = false;
            this.btnCheckIn.Click += new System.EventHandler(this.btnCheckIn_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::HotelMNS_WF.Properties.Resources.search1;
            this.pictureBox2.Location = new System.Drawing.Point(897, 60);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(23, 18);
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::HotelMNS_WF.Properties.Resources.setting3;
            this.pictureBox1.Location = new System.Drawing.Point(177, 13);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(37, 35);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // btnCheckOut
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.btnCheckIn);
            this.Controls.Add(this.btnCheckOutCustomer);
            this.Controls.Add(this.datepickCheckOut);
            this.Controls.Add(this.datepickCheckIn);
            this.Controls.Add(this.txtIdCardType);
            this.Controls.Add(this.dgvListRoom);
            this.Controls.Add(this.lbRoomNumber);
            this.Controls.Add(this.materialLabel13);
            this.Controls.Add(this.txtCitizenship);
            this.Controls.Add(this.materialLabel12);
            this.Controls.Add(this.txtNotes);
            this.Controls.Add(this.txtPhoneNumber);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.txtIdCard);
            this.Controls.Add(this.txtBookingId);
            this.Controls.Add(this.materialLabel11);
            this.Controls.Add(this.radFemale);
            this.Controls.Add(this.radMale);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnEditInfo);
            this.Controls.Add(this.chkboxStDirty);
            this.Controls.Add(this.chkboxStCleaning);
            this.Controls.Add(this.chkboxStOccupied);
            this.Controls.Add(this.chkboxStCusWalking);
            this.Controls.Add(this.chkboxStFixing);
            this.Controls.Add(this.chkboxStAvaiable);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.materialLabel9);
            this.Controls.Add(this.materialLabel8);
            this.Controls.Add(this.materialLabel7);
            this.Controls.Add(this.materialLabel6);
            this.Controls.Add(this.materialLabel5);
            this.Controls.Add(this.materialLabel4);
            this.Controls.Add(this.materialLabel3);
            this.Controls.Add(this.materialLabel2);
            this.Controls.Add(this.txtCustomerName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.materialLabel1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.txtSearchRoom);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Name = "btnCheckOut";
            this.Size = new System.Drawing.Size(1168, 570);
            this.Load += new System.EventHandler(this._Room_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvListRoom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private MetroSuite.MetroTextBox txtSearchRoom;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label2;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtCustomerName;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private MaterialSkin.Controls.MaterialLabel materialLabel4;
        private MaterialSkin.Controls.MaterialLabel materialLabel5;
        private MaterialSkin.Controls.MaterialLabel materialLabel6;
        private MaterialSkin.Controls.MaterialLabel materialLabel7;
        private MaterialSkin.Controls.MaterialLabel materialLabel8;
        private MaterialSkin.Controls.MaterialLabel materialLabel9;
        private System.Windows.Forms.Label label4;
        private MaterialSkin.Controls.MaterialCheckBox chkboxStAvaiable;
        private MaterialSkin.Controls.MaterialCheckBox chkboxStFixing;
        private MaterialSkin.Controls.MaterialCheckBox chkboxStCusWalking;
        private MaterialSkin.Controls.MaterialCheckBox chkboxStDirty;
        private MaterialSkin.Controls.MaterialCheckBox chkboxStCleaning;
        private MaterialSkin.Controls.MaterialCheckBox chkboxStOccupied;
        private System.Windows.Forms.Button btnEditInfo;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label5;
        private MetroFramework.Controls.MetroRadioButton radMale;
        private MetroFramework.Controls.MetroRadioButton radFemale;
        private MaterialSkin.Controls.MaterialLabel materialLabel11;
        private System.Windows.Forms.TextBox txtBookingId;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtIdCard;
        private System.Windows.Forms.TextBox txtPhoneNumber;
        private System.Windows.Forms.TextBox txtNotes;
        private MaterialSkin.Controls.MaterialLabel materialLabel12;
        private System.Windows.Forms.TextBox txtCitizenship;
        private MaterialSkin.Controls.MaterialLabel materialLabel13;
        private MaterialSkin.Controls.MaterialLabel lbRoomNumber;
        private System.Windows.Forms.DataGridView dgvListRoom;
        private System.Windows.Forms.TextBox txtIdCardType;
        private MetroFramework.Controls.MetroDateTime datepickCheckIn;
        private MetroFramework.Controls.MetroDateTime datepickCheckOut;
        private System.Windows.Forms.Button btnCheckOutCustomer;
        private System.Windows.Forms.Button btnCheckIn;
    }
}
