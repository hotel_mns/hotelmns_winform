﻿namespace HotelMNS_WF.Shared.Dashboard
{
    partial class _MainDashboard
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lbUserCount = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lbTimeNow = new System.Windows.Forms.Label();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.label32 = new System.Windows.Forms.Label();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.lbDateNow = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.lbDescriptionweather = new System.Windows.Forms.Label();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.label17 = new System.Windows.Forms.Label();
            this.lbtempuarate = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.label30 = new System.Windows.Forms.Label();
            this.lbFacilityCount = new System.Windows.Forms.Label();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.lbServiceCount = new System.Windows.Forms.Label();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.label25 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.lbBookingCount = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.label12 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.label22 = new System.Windows.Forms.Label();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.lbRoomCount = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.lbCustomerCount = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(67)))), ((int)(((byte)(54)))));
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.lbUserCount);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(38, 63);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(246, 108);
            this.panel1.TabIndex = 19;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::HotelMNS_WF.Properties.Resources.userdashboard;
            this.pictureBox2.Location = new System.Drawing.Point(204, 29);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(30, 23);
            this.pictureBox2.TabIndex = 27;
            this.pictureBox2.TabStop = false;
            // 
            // lbUserCount
            // 
            this.lbUserCount.AutoSize = true;
            this.lbUserCount.Font = new System.Drawing.Font("Roboto", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUserCount.ForeColor = System.Drawing.Color.White;
            this.lbUserCount.Location = new System.Drawing.Point(169, 30);
            this.lbUserCount.Name = "lbUserCount";
            this.lbUserCount.Size = new System.Drawing.Size(32, 23);
            this.lbUserCount.TabIndex = 10;
            this.lbUserCount.Text = "40";
            this.lbUserCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Roboto", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(27, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 23);
            this.label1.TabIndex = 9;
            this.label1.Text = "User";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::HotelMNS_WF.Properties.Resources.groupuser2;
            this.pictureBox1.Location = new System.Drawing.Point(21, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(73, 65);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(81)))), ((int)(((byte)(181)))));
            this.panel5.Controls.Add(this.lbTimeNow);
            this.panel5.Controls.Add(this.pictureBox14);
            this.panel5.Controls.Add(this.label32);
            this.panel5.Controls.Add(this.pictureBox15);
            this.panel5.Controls.Add(this.lbDateNow);
            this.panel5.Location = new System.Drawing.Point(854, 183);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(246, 108);
            this.panel5.TabIndex = 26;
            // 
            // lbTimeNow
            // 
            this.lbTimeNow.AutoSize = true;
            this.lbTimeNow.Font = new System.Drawing.Font("Roboto", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTimeNow.ForeColor = System.Drawing.Color.White;
            this.lbTimeNow.Location = new System.Drawing.Point(172, 55);
            this.lbTimeNow.Name = "lbTimeNow";
            this.lbTimeNow.Size = new System.Drawing.Size(57, 15);
            this.lbTimeNow.TabIndex = 53;
            this.lbTimeNow.Text = "17:04:20";
            // 
            // pictureBox14
            // 
            this.pictureBox14.Image = global::HotelMNS_WF.Properties.Resources.Calendar;
            this.pictureBox14.Location = new System.Drawing.Point(16, 13);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(73, 65);
            this.pictureBox14.TabIndex = 48;
            this.pictureBox14.TabStop = false;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Roboto", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.White;
            this.label32.Location = new System.Drawing.Point(12, 81);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(86, 23);
            this.label32.TabIndex = 49;
            this.label32.Text = "Calendar";
            // 
            // pictureBox15
            // 
            this.pictureBox15.Image = global::HotelMNS_WF.Properties.Resources.clock;
            this.pictureBox15.Location = new System.Drawing.Point(144, 55);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(27, 21);
            this.pictureBox15.TabIndex = 51;
            this.pictureBox15.TabStop = false;
            // 
            // lbDateNow
            // 
            this.lbDateNow.AutoSize = true;
            this.lbDateNow.Font = new System.Drawing.Font("Roboto", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDateNow.ForeColor = System.Drawing.Color.White;
            this.lbDateNow.Location = new System.Drawing.Point(131, 30);
            this.lbDateNow.Name = "lbDateNow";
            this.lbDateNow.Size = new System.Drawing.Size(112, 23);
            this.lbDateNow.TabIndex = 50;
            this.lbDateNow.Text = "17/04/2016";
            this.lbDateNow.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(230)))), ((int)(((byte)(118)))));
            this.panel4.Controls.Add(this.label19);
            this.panel4.Controls.Add(this.label18);
            this.panel4.Controls.Add(this.lbDescriptionweather);
            this.panel4.Controls.Add(this.pictureBox7);
            this.panel4.Controls.Add(this.label17);
            this.panel4.Controls.Add(this.lbtempuarate);
            this.panel4.Location = new System.Drawing.Point(854, 63);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(246, 108);
            this.panel4.TabIndex = 22;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Roboto", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(191, 22);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(13, 14);
            this.label19.TabIndex = 49;
            this.label19.Text = "o";
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(196, 34);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(25, 16);
            this.label18.TabIndex = 48;
            this.label18.Text = "C";
            // 
            // lbDescriptionweather
            // 
            this.lbDescriptionweather.AutoSize = true;
            this.lbDescriptionweather.Font = new System.Drawing.Font("Roboto", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDescriptionweather.ForeColor = System.Drawing.Color.White;
            this.lbDescriptionweather.Location = new System.Drawing.Point(140, 54);
            this.lbDescriptionweather.Name = "lbDescriptionweather";
            this.lbDescriptionweather.Size = new System.Drawing.Size(63, 15);
            this.lbDescriptionweather.TabIndex = 47;
            this.lbDescriptionweather.Text = "Light Rain";
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::HotelMNS_WF.Properties.Resources.weather;
            this.pictureBox7.Location = new System.Drawing.Point(16, 12);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(73, 65);
            this.pictureBox7.TabIndex = 42;
            this.pictureBox7.TabStop = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Roboto", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(12, 80);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(81, 23);
            this.label17.TabIndex = 43;
            this.label17.Text = "Weather";
            // 
            // lbtempuarate
            // 
            this.lbtempuarate.AutoSize = true;
            this.lbtempuarate.Font = new System.Drawing.Font("Roboto", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbtempuarate.ForeColor = System.Drawing.Color.White;
            this.lbtempuarate.Location = new System.Drawing.Point(140, 29);
            this.lbtempuarate.Name = "lbtempuarate";
            this.lbtempuarate.Size = new System.Drawing.Size(32, 23);
            this.lbtempuarate.TabIndex = 44;
            this.lbtempuarate.Text = "26";
            this.lbtempuarate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(39)))), ((int)(((byte)(176)))));
            this.panel6.Controls.Add(this.pictureBox12);
            this.panel6.Controls.Add(this.label30);
            this.panel6.Controls.Add(this.lbFacilityCount);
            this.panel6.Controls.Add(this.pictureBox13);
            this.panel6.Location = new System.Drawing.Point(579, 183);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(246, 108);
            this.panel6.TabIndex = 25;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = global::HotelMNS_WF.Properties.Resources.hotel_facility;
            this.pictureBox12.Location = new System.Drawing.Point(15, 13);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(73, 65);
            this.pictureBox12.TabIndex = 42;
            this.pictureBox12.TabStop = false;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Roboto", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.White;
            this.label30.Location = new System.Drawing.Point(17, 81);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(71, 23);
            this.label30.TabIndex = 43;
            this.label30.Text = "Facility";
            // 
            // lbFacilityCount
            // 
            this.lbFacilityCount.AutoSize = true;
            this.lbFacilityCount.Font = new System.Drawing.Font("Roboto", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFacilityCount.ForeColor = System.Drawing.Color.White;
            this.lbFacilityCount.Location = new System.Drawing.Point(160, 30);
            this.lbFacilityCount.Name = "lbFacilityCount";
            this.lbFacilityCount.Size = new System.Drawing.Size(32, 23);
            this.lbFacilityCount.TabIndex = 44;
            this.lbFacilityCount.Text = "10";
            this.lbFacilityCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox13
            // 
            this.pictureBox13.Image = global::HotelMNS_WF.Properties.Resources.equiment;
            this.pictureBox13.Location = new System.Drawing.Point(198, 30);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(30, 23);
            this.pictureBox13.TabIndex = 45;
            this.pictureBox13.TabStop = false;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(152)))), ((int)(((byte)(0)))));
            this.panel7.Controls.Add(this.pictureBox10);
            this.panel7.Controls.Add(this.lbServiceCount);
            this.panel7.Controls.Add(this.pictureBox11);
            this.panel7.Controls.Add(this.label25);
            this.panel7.Location = new System.Drawing.Point(307, 183);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(246, 108);
            this.panel7.TabIndex = 24;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = global::HotelMNS_WF.Properties.Resources.room_service;
            this.pictureBox10.Location = new System.Drawing.Point(19, 13);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(73, 65);
            this.pictureBox10.TabIndex = 36;
            this.pictureBox10.TabStop = false;
            // 
            // lbServiceCount
            // 
            this.lbServiceCount.AutoSize = true;
            this.lbServiceCount.Font = new System.Drawing.Font("Roboto", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbServiceCount.ForeColor = System.Drawing.Color.White;
            this.lbServiceCount.Location = new System.Drawing.Point(167, 30);
            this.lbServiceCount.Name = "lbServiceCount";
            this.lbServiceCount.Size = new System.Drawing.Size(32, 23);
            this.lbServiceCount.TabIndex = 38;
            this.lbServiceCount.Text = "10";
            this.lbServiceCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = global::HotelMNS_WF.Properties.Resources.service_room;
            this.pictureBox11.Location = new System.Drawing.Point(202, 30);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(30, 23);
            this.pictureBox11.TabIndex = 39;
            this.pictureBox11.TabStop = false;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Roboto", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.White;
            this.label25.Location = new System.Drawing.Point(18, 81);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(74, 23);
            this.label25.TabIndex = 37;
            this.label25.Text = "Service";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(30)))), ((int)(((byte)(99)))));
            this.panel3.Controls.Add(this.pictureBox5);
            this.panel3.Controls.Add(this.lbBookingCount);
            this.panel3.Controls.Add(this.pictureBox6);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Location = new System.Drawing.Point(579, 63);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(246, 108);
            this.panel3.TabIndex = 21;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::HotelMNS_WF.Properties.Resources.Booking;
            this.pictureBox5.Location = new System.Drawing.Point(15, 12);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(73, 65);
            this.pictureBox5.TabIndex = 36;
            this.pictureBox5.TabStop = false;
            // 
            // lbBookingCount
            // 
            this.lbBookingCount.AutoSize = true;
            this.lbBookingCount.Font = new System.Drawing.Font("Roboto", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbBookingCount.ForeColor = System.Drawing.Color.White;
            this.lbBookingCount.Location = new System.Drawing.Point(174, 29);
            this.lbBookingCount.Name = "lbBookingCount";
            this.lbBookingCount.Size = new System.Drawing.Size(21, 23);
            this.lbBookingCount.TabIndex = 38;
            this.lbBookingCount.Text = "1";
            this.lbBookingCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::HotelMNS_WF.Properties.Resources.pencil;
            this.pictureBox6.Location = new System.Drawing.Point(199, 29);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(27, 24);
            this.pictureBox6.TabIndex = 39;
            this.pictureBox6.TabStop = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Roboto", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(11, 80);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(81, 23);
            this.label12.TabIndex = 37;
            this.label12.Text = "Booking";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(195)))), ((int)(((byte)(74)))));
            this.panel8.Controls.Add(this.pictureBox9);
            this.panel8.Controls.Add(this.label22);
            this.panel8.Controls.Add(this.pictureBox8);
            this.panel8.Controls.Add(this.lbRoomCount);
            this.panel8.Location = new System.Drawing.Point(38, 183);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(246, 108);
            this.panel8.TabIndex = 23;
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = global::HotelMNS_WF.Properties.Resources.bed3;
            this.pictureBox9.Location = new System.Drawing.Point(21, 13);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(73, 65);
            this.pictureBox9.TabIndex = 30;
            this.pictureBox9.TabStop = false;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Roboto", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.White;
            this.label22.Location = new System.Drawing.Point(27, 81);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(60, 23);
            this.label22.TabIndex = 31;
            this.label22.Text = "Room";
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::HotelMNS_WF.Properties.Resources.bed4;
            this.pictureBox8.Location = new System.Drawing.Point(204, 30);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(30, 23);
            this.pictureBox8.TabIndex = 33;
            this.pictureBox8.TabStop = false;
            // 
            // lbRoomCount
            // 
            this.lbRoomCount.AutoSize = true;
            this.lbRoomCount.Font = new System.Drawing.Font("Roboto", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbRoomCount.ForeColor = System.Drawing.Color.White;
            this.lbRoomCount.Location = new System.Drawing.Point(169, 30);
            this.lbRoomCount.Name = "lbRoomCount";
            this.lbRoomCount.Size = new System.Drawing.Size(32, 23);
            this.lbRoomCount.TabIndex = 32;
            this.lbRoomCount.Text = "10";
            this.lbRoomCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(169)))), ((int)(((byte)(244)))));
            this.panel2.Controls.Add(this.pictureBox4);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.pictureBox3);
            this.panel2.Controls.Add(this.lbCustomerCount);
            this.panel2.Location = new System.Drawing.Point(307, 63);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(246, 108);
            this.panel2.TabIndex = 20;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::HotelMNS_WF.Properties.Resources.customerhuman;
            this.pictureBox4.Location = new System.Drawing.Point(19, 12);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(73, 65);
            this.pictureBox4.TabIndex = 30;
            this.pictureBox4.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Roboto", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(15, 80);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(93, 23);
            this.label9.TabIndex = 31;
            this.label9.Text = "Customer";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::HotelMNS_WF.Properties.Resources.customerhuman2;
            this.pictureBox3.Location = new System.Drawing.Point(202, 29);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(30, 23);
            this.pictureBox3.TabIndex = 33;
            this.pictureBox3.TabStop = false;
            // 
            // lbCustomerCount
            // 
            this.lbCustomerCount.AutoSize = true;
            this.lbCustomerCount.Font = new System.Drawing.Font("Roboto", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCustomerCount.ForeColor = System.Drawing.Color.White;
            this.lbCustomerCount.Location = new System.Drawing.Point(178, 30);
            this.lbCustomerCount.Name = "lbCustomerCount";
            this.lbCustomerCount.Size = new System.Drawing.Size(21, 23);
            this.lbCustomerCount.TabIndex = 32;
            this.lbCustomerCount.Text = "1";
            this.lbCustomerCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Roboto", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(14, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(146, 33);
            this.label5.TabIndex = 30;
            this.label5.Text = "Dashboard";
            // 
            // _MainDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.label5);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel2);
            this.Name = "_MainDashboard";
            this.Size = new System.Drawing.Size(1168, 686);
            this.Load += new System.EventHandler(this._MainDashboard_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbUserCount;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label lbCustomerCount;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label lbBookingCount;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lbDescriptionweather;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lbtempuarate;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.Label lbServiceCount;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Label lbRoomCount;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label lbFacilityCount;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.Label lbTimeNow;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.Label lbDateNow;
    }
}
