﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS;
using DTA;
using System.Collections;

namespace HotelMNS_WF
{
    public partial class frmAddService : MetroFramework.Forms.MetroForm
    {
        RoomMapBUS roomlist = new RoomMapBUS();
        BindingSource source = new BindingSource();
        int roomid;
        int bookingid;
        int index;
        int servicecount = 0;
        decimal totalprice = 0;
        int indexroom;
        public frmAddService(int idroom, int idbooking)
        {
            roomid = idroom;
            bookingid = idbooking;
            InitializeComponent();
        }
        public void Init()
        {
            dgvListService.AutoGenerateColumns = false;
            dgvListService.Columns.Add("clsservicename", "Service");
            dgvListService.Columns[0].DataPropertyName = "name";

            dgvListService.Columns.Add("clsserviceprice", "Prices");
            dgvListService.Columns[1].DataPropertyName = "price";
            dgvListService.Columns[1].Visible = false;

            dgvListService.Columns.Add("clsserviceid", "Id");
            dgvListService.Columns[2].DataPropertyName = "id";
            dgvListService.Columns[2].Visible = false;

            dgvListServiceRoom.AutoGenerateColumns = false;
            dgvListServiceRoom.Columns.Add("clsservicename", "Service");

            dgvListServiceRoom.Columns.Add("clsserviceprice", "Prices");

            dgvListServiceRoom.Columns.Add("clsserviceid", "Id");
            dgvListServiceRoom.Columns[2].Visible = false;

            dgvListServiceRoom.Columns.Add("clsindexservice", "service Id");
            dgvListServiceRoom.Columns[3].Visible = false;
        }

        private void frmAddService_Load(object sender, EventArgs e)
        {
            Init();
            lbRoomid.Text = roomid.ToString();
            LoadService(roomid);
        }

        public void LoadService(int roomid)
        {
            List<spGetListServiceAvaiableBooking_Result> listservice = roomlist.GetlistServiceRoom(roomid);
            dgvListService.DataSource = listservice;
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvListService_SelectionChanged(object sender, EventArgs e)
        {
            index = dgvListService.CurrentCell.RowIndex;
            txtPrice.Text = dgvListService.Rows[index].Cells[1].Value.ToString() + " $";
        }

        private void btnAddService_Click(object sender, EventArgs e)
        {
            ArrayList row = new ArrayList();
            string pricename = dgvListService.Rows[index].Cells[0].Value.ToString();
            decimal price = Decimal.Parse(dgvListService.Rows[index].Cells[1].Value.ToString());
            int serviceid = Int32.Parse(dgvListService.Rows[index].Cells[2].Value.ToString());
            row.Add(pricename);
            row.Add(price);
            row.Add(serviceid);
            row.Add(index);
            dgvListServiceRoom.Rows.Add(row.ToArray());
            CurrencyManager currencyManager1 = (CurrencyManager)BindingContext[dgvListService.DataSource];
            currencyManager1.SuspendBinding();
            dgvListService.Rows[index].Visible = false;
            currencyManager1.ResumeBinding();
            servicecount++;
            totalprice += price;
            txtTotalService.Text = servicecount.ToString();
            txtTotalPrice.Text = totalprice.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            servicecount--;
            totalprice -= Decimal.Parse(dgvListServiceRoom.Rows[indexroom].Cells[1].Value.ToString());
            txtTotalService.Text = servicecount.ToString();
            txtTotalPrice.Text = totalprice.ToString();
            int i = Int32.Parse(dgvListServiceRoom.Rows[indexroom].Cells[3].Value.ToString());
            dgvListServiceRoom.Rows.RemoveAt(indexroom);
            CurrencyManager currencyManager1 = (CurrencyManager)BindingContext[dgvListService.DataSource];
            currencyManager1.SuspendBinding();
            dgvListService.Rows[i].Visible = true;
            currencyManager1.ResumeBinding();
        }

        private void dgvListServiceRoom_SelectionChanged(object sender, EventArgs e)
        {
            indexroom = dgvListServiceRoom.CurrentCell.RowIndex;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if(MetroFramework.MetroMessageBox.Show(this,"Do you want Confirm ","Warning ", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
            {
                frmPayment payment = new frmPayment(totalprice);
                if (payment.ShowDialog() == DialogResult.OK)
                {
                    for (int i = 0; i < dgvListServiceRoom.Rows.Count-1; i++)
                    {
                        int serviceid = Int32.Parse(dgvListServiceRoom.Rows[i].Cells[2].Value.ToString());
                        roomlist.InsertNewBookingService(bookingid, roomid, serviceid);
                    }
                    decimal customerMoney = payment.moneylist[0];
                    decimal chanegMoney = payment.moneylist[1];
                    roomlist.UpdateInvoice(bookingid, servicecount, totalprice, customerMoney, chanegMoney);
                }
            }
            MetroFramework.MetroMessageBox.Show(this, "Booking New Service Succssfully", "Infomation", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
